import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
// import { NavigationContainer } from '@react-navigation/native'
// import { createStackNavigator } from '@react-navigation/stack'
// import Youtube from './tugas/tugas12/App'
// import Welcome from './tugas/tugas13/screens/Welcome'
// import SignUp from './tugas/tugas13/screens/SignUp'
// import Login from './tugas/tugas13/screens/Login'
// import About from './tugas/tugas13/screens/About'
// import Todo from './tugas/tugas14/App'
// import Skill from './tugas/tugas14/SkillScreen'
// import Tugas15 from './tugas/tugas15/index'
// import TugasNavigation from './tugas/tugasNavigation/index'
import Quiz from './tugas/Quiz3/index'

// const Stack = createStackNavigator()

export default function App() {
  return (
    ///tugas 12///
    // <Youtube />

    ///tugas 13///
    // <NavigationContainer>
    //   <Stack.Navigator>
    //     <Stack.Screen name="Welcome" options={{headerShown: false}} component={Welcome}></Stack.Screen>
    //     <Stack.Screen name="SignUp" component={SignUp}></Stack.Screen>
    //     <Stack.Screen name="Login" component={Login}></Stack.Screen>
    //     <Stack.Screen name="About" options={{headerShown: false}} component={About}></Stack.Screen>
    //   </Stack.Navigator>
    // </NavigationContainer>

    ///tugas 14///
    // <Todo />
    // <Skill />

    ///tugas 15///
    // <Tugas15 />
    // <TugasNavigation />

    <Quiz />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
