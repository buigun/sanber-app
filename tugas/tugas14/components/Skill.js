import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Icon2 from 'react-native-vector-icons/MaterialIcons'

export default function Skill({ val }) {
    return (
        <TouchableOpacity>
            <View style={styles.container}>
                <View>
                    <Icon name={val.iconName} color="#003366" size={100} />
                </View>
                <View>
                    <Text style={{fontSize:24, color: '#003366', fontWeight: 'bold'}}>{val.skillName}</Text>
                    <Text style={{fontSize:16, color: '#3EC6FF'}}>{val.categoryName}</Text>
                    <Text style={{fontSize:48, color: '#FFFFFF', textAlign:'right'}}>{val.percentageProgress}</Text>
                </View>
                <View>
                    <Icon2 name="navigate-next" color="#003366" size={100} />
                </View>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 10,
        borderRadius: 8,
        backgroundColor: '#B4E9FF',
        marginVertical: 10,
        marginHorizontal: 16,
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
})