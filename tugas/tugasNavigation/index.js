import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from '@react-navigation/drawer'

import { Login, SignUp, About, Skill, Project, Add } from './screens'

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tabs = createBottomTabNavigator();

const SkillTabs = () => (
    <Tabs.Navigator>
        <Tabs.Screen name='Skill' component={Skill} />
        <Tabs.Screen name='Project' component={Project} />
        <Tabs.Screen name='Add' component={Add} />
    </Tabs.Navigator>
)

const AboutDrawer = () => (
    <Drawer.Navigator>
        <Drawer.Screen name='About' component={About} />
        <Drawer.Screen name='Skill' component={SkillTabs} />
    </Drawer.Navigator>
)

export default () => (
    <NavigationContainer>
        <Stack.Navigator>
            <Stack.Screen name='Login' component={Login} options={{headerShown: false}} />
            <Stack.Screen name='SignUp' component={SignUp} options={{headerShown: false}} />
            <Stack.Screen name='About' component={AboutDrawer} />
        </Stack.Navigator>
    </NavigationContainer>
)