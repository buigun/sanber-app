import React from 'react'
import { StyleSheet, View, Image, Text, TouchableOpacity, ScrollView} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import data from '../../tugas14/skillData.json'
import Skill from '../../tugas14/components/Skill'

export default function SkillScreen() {
    let skills = data.items.map((val, key) => {
        return <Skill 
          key={key} val={val} 
        />
    });

    return (
        <View style={styles.container}>
            <Image style={{height: 60, width: 188, marginBottom: 20, alignSelf:"flex-end" }} source={require("../../tugas14/assets/sanber.png")}/>
            <View style={{flexDirection:"row"}}>
                <Icon name="account-circle" color="#3EC6FF" style={{marginLeft: 16}} size={32} />
                <View style={{marginLeft: 8}}>
                    <Text style={{fontSize: 12}}>Hai</Text>
                    <Text style={{fontSize: 16}}>Mukhlis Hanafi</Text>
                </View>
            </View>
            <View style={{marginHorizontal: 16, marginTop: 16, borderColor: '#3EC6FF', borderBottomWidth: 4}}>
                <Text style={{fontSize:36, color: '#003366'}}>SKILL</Text>
            </View>
            <View style={{marginHorizontal: 16, marginVertical: 10, flexDirection: "row", justifyContent: "space-around"}}>
                <View style={styles.category}>
                    <Text>Library/Framework</Text>
                </View>
                <View style={styles.category}>
                    <Text>Bahasa Pemrograman</Text>
                </View>
                <View style={styles.category}>
                    <Text>Teknologi</Text>
                </View>
            </View>
            <ScrollView>
                {skills}
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex : 1,
        backgroundColor: 'white'
    },
    category: {
        padding: 6,
        backgroundColor: '#B4E9FF',
        borderRadius: 8
    }
})