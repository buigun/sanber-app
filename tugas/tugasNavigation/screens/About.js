import React from 'react'
import { StyleSheet, View, Image, Text, TouchableOpacity, Linking} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default function About({ navigation }) {
    return (
        <View style={styles.container}>
            <View style={styles.body}>
                <Image style={{height: 80, width: 80, marginBottom: 20 }} source={require("../../tugas13/assets/logo.png")}/>
                <Text style={{fontWeight:'bold', fontSize: 20}}>SanberApp</Text>
                <View style={styles.info}>
                    <Text>Version : v.1.0.0</Text>
                    <Text style={{marginTop: 8}}>Release : 29th June 2020</Text>
                    <Text style={{marginTop: 8}}>Creator : Budi Indra Gunawan</Text>
                </View>
                <View style={styles.socmed}>
                    <Text style={{textAlign: 'center', fontSize: 20, marginBottom: 16}}> Contact Me</Text>
                    <View style={styles.list}>
                        <TouchableOpacity
                            onPress={()=> Linking.openURL('https://web.facebook.com/buigun?_rdc=1&_rdr')}
                        >
                            <Image style={{height: 64, width: 64, marginBottom: 20 }} source={require("../../tugas13/assets/fb.png")}/>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={()=> Linking.openURL('https://www.instagram.com/buigun/')}
                        >
                            <Image style={{height: 64, width: 64, marginBottom: 20 }} source={require("../../tugas13/assets/insta.png")}/>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={()=> Linking.openURL('https://twitter.com/buigun')}
                        >
                            <Image style={{height: 64, width: 64, marginBottom: 20 }} source={require("../../tugas13/assets/twitter.png")}/>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={()=> Linking.openURL('https://github.com/buigun')}
                        >
                            <Image style={{height: 64, width: 64, marginBottom: 20 }} source={require("../../tugas13/assets/github.png")}/>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex : 1,
        marginTop: 80,
        backgroundColor: "white"
    },
    body: {
        flex : 1,
        alignItems: "center",
        justifyContent: "center"
    },
    info: {
        borderColor: 'grey',
        borderStyle: 'solid',
        borderWidth: 0.5,
        borderRadius: 5,
        padding: 32,
        marginTop: 24
    },
    socmed: {
        flex: 1,
        marginTop: 24,
        width: '100%'
    },
    list: {
        flexDirection: "row",
        justifyContent: "space-around"
    }
})