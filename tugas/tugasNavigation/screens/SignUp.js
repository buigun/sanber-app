import React from 'react'
import { StyleSheet, View, Button, TextInput, Image, Text } from 'react-native'

export default function SignUp({ navigation }) {
    return (
        <View style={styles.container}>
            <Image style={{height: 80, width: 80, marginBottom: 20 }} source={require("../../tugas13/assets/logo.png")}/>
            <TextInput style={styles.input} placeholder="username" textContentType="username"></TextInput>
            <TextInput style={styles.input} placeholder="email" textContentType="emailAddress"></TextInput>
            <TextInput style={styles.input} placeholder="password" textContentType="password" secureTextEntry={true}></TextInput>
            <TextInput style={styles.input} placeholder="confirm password" textContentType="password" secureTextEntry={true} ></TextInput>
            <View style={{marginTop: 32, width: '80%'}}>
                <Button onPress={()=>navigation.navigate("About")} title="SIGN UP" color="#00ADB5"></Button>
            </View>
            <Text style={{marginTop:40}}>
                <Text>Already have an Account?</Text>
                <Text style={{fontWeight:'bold', fontSize:16,}} onPress={()=>navigation.navigate("Login")}> Login</Text>
            </Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex : 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "white"
    },
    form:{
        marginTop: 40
    },
    input: {
        borderWidth: 1,
        borderColor: '#393E46',
        paddingLeft: 16,
        padding: 8,
        width: '80%',
        borderRadius: 5,
        marginTop: 16,
    }
})