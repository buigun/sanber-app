import React from 'react'
import { StyleSheet, View, Button, Text, Image } from 'react-native'

export default function Welcome({ navigation }) {
 return (
     <View style={styles.container}>
         <Image style={{height: 260, width: 260 }} source={require("../assets/code.gif")}/>
         <Text style={styles.welcome}>Welcome</Text>
         <View style={styles.info}>
            <Text style={{textAlign:"center"}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>
         </View>
         <View style={{marginTop: 60, width: 160}}>
            <Button onPress={()=>navigation.navigate("SignUp")} title="SIGN UP" color="#00ADB5"></Button>
         </View>
         <View style={{marginTop: 20, width: 160}}>
            <Button onPress={()=>navigation.navigate("Login")} title="LOGIN" color="#00ADB5"></Button>
         </View>
        
        {/* <Button style={styles.button} title="LOGIN"></Button> */}
     </View>
 )
}

const styles = StyleSheet.create({
    container: {
        flex : 1,
        alignItems: "center",
        justifyContent: "center"
    },
    welcome: {
        fontWeight: "bold",
        fontSize: 32,
        marginBottom: 20
    },
    info: {
        width: 300
    }
})